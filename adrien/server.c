#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "functions.h"

/*It prints the adress of the client*/
static void display_addr(struct sockaddr_in * sac)
{
  char *s;
  s = inet_ntoa(sac->sin_addr);
  if(!s)
  {
    return ;
  }
  printf("%s \n", s);
}

/*We try to accept a client */
static int accept_client(int fd)
{
  int r;
  struct sockaddr_in sac;
  socklen_t client_len = sizeof(sac);
  r = accept(fd, (struct sockaddr *)&sac, &client_len);
  if (r < 0)
  {
    fatal_error("accept failed");
  }
  display_addr(&sac);
  return r;
}

/*we create an echo from the text of the client. It can't be more than 1024 character*/
static void handle_client(int client_fd)
{
  char received[SIZE];
  int size = receive(client_fd, received);
  print(received, size);
  send_fd(client_fd, received, size);
}

int main(int argc, char * argv[])
{
  struct sockaddr_in SA;
  int fd;
  int r;
  if (argc == 1)
  {
    fatal_error("no port");
  }
  else
  {
    SA.sin_family = AF_INET;
    SA.sin_addr.s_addr = htonl(INADDR_ANY);
    SA.sin_port = htons(atoi(argv[1]));
  }
  
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (fd < 0)
  {
    fatal_error("socket failed");
  }
  
  r = bind(fd, (struct sockaddr*)&SA, sizeof(SA));
  if (r < 0)
  {
    fatal_error("bind failed");
  }
  
  int l = listen(fd, 1);
  if(l < 0)
  {
    fatal_error("listen failed");
  }
  printf("Waiting for a client ...\n");
  int client_fd;
  client_fd = accept_client(fd);
  while(1)
  {
    handle_client(client_fd);
  }
  
  return 0;
}


