#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "functions.h"

/*This sends a text to the server and waits the answer*/
static void handle_server(int server_fd, char *text)
{
  send_fd(server_fd, text, strlen(text));
  char received[SIZE];
  int size = receive(server_fd, received);
  printf("You have received : ");
  print(received, size);
}


int main(int argc, char * argv[])
{
  struct sockaddr_in SA;
  int fd;
  int test = 1;
  char text[SIZE];
  
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (fd < 0)
  {
    fatal_error("socket failed");
  }
  
  /*server*/
  if (argc <= 2)
  {
    fatal_error("no adress or no port");
  }
  else
  {
    SA.sin_family = AF_INET;
    SA.sin_addr.s_addr = inet_addr(argv[1]);
    SA.sin_port = htons(atoi(argv[2]));
  }
  
  if (connect(fd, (struct sockaddr *)&SA, sizeof SA) < 0)
  {
    fatal_error("conect failed");
  }
  
  while (test)
  {
    printf("Enter your message : ");
    fflush(stdout);
    scanf("%s", text);
    printf("\n");
    fflush(stdout);
    handle_server(fd, text); 
  }
  return 0;
}

