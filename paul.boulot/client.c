#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "functions.h"
#include "7colors.h"

/*This sends a text to the server and waits the answer*/
static char * receive_board(int server_fd)
{
  char * board = malloc(BOARD_SIZE * BOARD_SIZE);
  int size = receive(server_fd, board);
  if  (size < 1)
  {
    fatal_error("The server do not respond, try again in a few minutes");
  }
  return board;
}


int main(int argc, char * argv[])
{
  struct sockaddr_in SA;
  int fd;
  
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (fd < 0)
  {
    fatal_error("socket failed");
  }
  
  /*server*/
  if (argc <= 2)
  {
    fatal_error("no adress or no port");
  }
  else
  {
    SA.sin_family = AF_INET;
    SA.sin_addr.s_addr = inet_addr(argv[1]);
    SA.sin_port = htons(atoi(argv[2]));
  }
  
  if (connect(fd, (struct sockaddr *)&SA, sizeof SA) < 0)
  {
    fatal_error("conect failed");
  }
  
  int test = 1;
  while(test)
  {
    char * board = receive_board(fd);
    print_board(board);
    printf("\n");
  }
  return 0;
}

