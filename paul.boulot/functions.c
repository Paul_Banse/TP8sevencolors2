#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "functions.h"
/*This prints an array */
void print(char tab[], int size)
{
  int i = 0;
  while(i<size)
  {
    printf("%c",tab[i]);
    i++;
  }
  printf("\n");
}

/*When a function failed, we called this function to exit the program with the right error text*/
void fatal_error(const char * reason)
{
  perror(reason);
  exit(1);
}

/*It sends a text to the adress designed by the file descriptor fd*/
void send_fd(int fd, char * text, int size)
{
  int todo = size;
  char * p = text;
  while (todo)
  {
    int sent = send(fd, p, todo, 0);
    if (sent < 0)
    {
      fatal_error("send failled");
    }
    p += sent;
    todo -= sent;
  }
}

/*It receives a text from the adress designed by fd*/
int receive(int fd, char * received)
{
  int got = recv(fd, received, SIZE-1, 0);
  if (got < 0)
  {
    fatal_error("recv failled");
  }
  received[got]='\0';
  return got;
}
