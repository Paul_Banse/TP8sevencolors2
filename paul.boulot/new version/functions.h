#ifndef FUNCTIONS_H
#define FUNCTIONS_H

/*the size of every text send or received are defined by SIZE*/
#define SIZE 1024
/*This prints an array*/
void print(char tab[], int size);

/*When a function failed, we called this function to exit the program with the right error text reason*/
void fatal_error(const char * reason);

/*It sends a text to the adress design by the file descriptor fd*/
void send_fd(int fd, char * text, int size);

/*It receives a text from the adress designed by fd*/
int receive(int fd, char * received);
#endif
