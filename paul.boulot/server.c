#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "functions.h"
#include "7colors.h"
/*It prints the adress of the client*/
static void display_addr(struct sockaddr_in * sac)
{
  char *s;
  s = inet_ntoa(sac->sin_addr);
  if(!s)
  {
    return ;
  }
  printf("%s \n", s);
}

/*We try to accept a client */
static int accept_client(int fd)
{
  int r;
  struct sockaddr_in sac;
  socklen_t client_len = sizeof(sac);
  r = accept(fd, (struct sockaddr *)&sac, &client_len);
  if (r < 0)
  {
    fatal_error("accept failed");
  }
  display_addr(&sac);
  return r;
}

/*we create an echo from the text of the client. It can't be more than 1024 character*/
static void send_board(int client_fd, struct Game * game)
{
  send_fd(client_fd, game->board, sizeof(game->board));
}

int main(int argc, char * argv[])
{
  struct sockaddr_in SA;
  int fd;
  int r;
  if (argc == 1)
  {
    fatal_error("no port");
  }
  else
  {
    SA.sin_family = AF_INET;
    SA.sin_addr.s_addr = htonl(INADDR_ANY);
    SA.sin_port = htons(atoi(argv[1]));
  }
  
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (fd < 0)
  {
    fatal_error("socket failed");
  }
  
  r = bind(fd, (struct sockaddr*)&SA, sizeof(SA));
  if (r < 0)
  {
    fatal_error("bind failed");
  }
  
  int l = listen(fd, 1);
  if(l < 0)
  {
    fatal_error("listen failed");
  }
  printf("Waiting for a client ...\n");
  int client_fd;
  client_fd = accept_client(fd);
  
  struct Game * game = init_game();
  send_board(client_fd, game);
  int not_end = 1;
  while(not_end)
  {
    game = seven_colors(game);
    game = is_over(game);
    not_end = 1+-1*(game -> end);
    send_board(client_fd, game);
  }
  return 0;
}


