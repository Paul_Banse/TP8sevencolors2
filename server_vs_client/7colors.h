#ifndef SCOLORS
#define SCOLORS

#define BOARD_SIZE 30
#define couleur(param1,param2) printf("\033[%d;%dm",param1,param2)

struct Player{
  int id;
  char type;
  int territory;
};

struct Game{
  char board[BOARD_SIZE * BOARD_SIZE];
  struct Player player1;
  struct Player player2;
  struct Player * current_player;
  int end ;  
};

char colors(int n);

int colorev( char color );

char get_cell(int x, int y, char board[]);

void set_cell(int x, int y, char color, char board[]);

void print_board(char board[]);

void init_board(char board[]);

int check_adj(int x, int y, char playercolor, char board[]);

void actualize_world(char chosenColor, struct Game * game);

double territory_percentage(struct Player *p);

void copy_board(char* b1,char* b2);

char ai_random(struct Game *game);

char ai_glutton(struct Game * game);

char ai_hegemony(struct Game * game);

void is_over(struct Game * game);

char ask_new_game();

struct Game * init_game(int dont_have, char board[]);

char my_turn(struct Game * game, struct Player * current_player);

char opponent_turn(struct Game * game, struct Player * current_player, char opponent_color);

void free_game(struct Game * game);

#endif
