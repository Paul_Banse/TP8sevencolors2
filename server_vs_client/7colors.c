/* Template of the 7 wonders of the world of the 7 colors assigment (from Anne-CÃ©cile Orgerie). */
#include <stdio.h>     /* printf */
#include <stdlib.h>  /*rand*/
#include <time.h>
#include "7colors.h"

/*links every integer with a color*/
char colors(int n)
{ if (n==0){
      return 'R';
    }
    if (n==1){
      return 'G';
    }
    if (n==2){
      return 'Y';
    }
    if (n==3){
      return 'B';
    }
    if (n==4){
      return 'P';
    }
    if (n==5){
      return 'C';
    }
    if (n==6){
      return 'W';
    }
    return '\n';
      }

/*links every color with an integer*/
int colorev( char color )
{
    if (color=='R'){
      return 0;
    }
    if (color=='G'){
      return 1;
    }
    if (color=='Y'){
      return 2;
    }
    if (color=='B'){
      return 3;
    }
    if (color=='P'){
      return 4;
    }
    if (color=='C'){
      return 5;
    }
    if (color=='W'){
      return 6;
    }
    return -1;
      }
/* Retrieves the color of a given board cell */
char get_cell(int x, int y, char board[])
{
  return board[y * BOARD_SIZE + x];
}

/* Changes the color of a given board cell */
void set_cell(int x, int y, char color, char board[])
{
  board[y * BOARD_SIZE + x] = color;
}

/* Prints the current state of the board on screen
 *
 * Implementation note: It would be nicer to do this with ncurse or even
 * SDL/allegro, but this is not really the purpose of this assignment.
 */
void print_board(char board[])
 
{     
  int i, j;
  for (i = 0; i < BOARD_SIZE; i++) {
    for (j = 0; j < BOARD_SIZE; j++) {
	if (get_cell(i,j,board)=='1' || get_cell(i,j,board)=='2'){
	    couleur(37,40+colorev(get_cell(i, j, board))+1);
	}
	else{
            couleur(30,40+colorev(get_cell(i, j, board))+1);
	}
            printf(" %c ", get_cell(i, j, board));
            couleur(0,0);
    }
        printf("\n");
  }
  printf("\n");
}


/*initialize the board*/
void init_board(char board[])
{
  int i, j;
  for (i = 0; i < BOARD_SIZE; i++) {
    for (j = 0; j < BOARD_SIZE; j++) {
            set_cell(i,j, colors(rand() % 7), board);
    }
    };
  set_cell(0,BOARD_SIZE-1,'1', board);
  set_cell(BOARD_SIZE-1,0,'2', board);
}

/* return 1 if a neighbour is the color*/
int check_adj(int x, int y, char playercolor, char board[])
{
  if (x != 0)
    {
      if (get_cell(x-1,y,board) == playercolor) {return 1;}
    }
  if (x != BOARD_SIZE - 1)
    {
      if (get_cell(x+1,y,board) == playercolor) {return 1;}
    }
  if (y != 0)
    {
      if (get_cell(x,y-1,board) == playercolor) {return 1;}
    }
  if (y != BOARD_SIZE - 1)
    {
      if (get_cell(x,y+1,board) == playercolor) {return 1;}
    }
  return 0;
}

/*actualize the board after an input*/
void actualize_world(char chosenColor, struct Game * game)
{
  char playerColor;
  struct Player * player;
  if ((*(game->current_player)).id == 1) {
	  playerColor = '1';
	  player = &(game-> player1);
	}   
  else {
	  playerColor = '2';
	  player= &(game-> player2);
	  }
  int i, j ;
  int bool =1;
  while (bool)
    {
      bool=0;
      for (i = 0; i < BOARD_SIZE; i++) {
    for (j = 0; j < BOARD_SIZE; j++) {
      if (get_cell(i, j, game->board)== chosenColor && check_adj(i, j, playerColor, game->board))
        {  
                    bool=1;
                    set_cell(i, j, playerColor, game->board);
                    (*player).territory = (*player).territory + 1;

        };
    }
      }
    }

 
}

/* computate a player’s territory percentage */
double territory_percentage(struct Player *p)
{
  return (((double) p->territory)/((double) BOARD_SIZE*BOARD_SIZE));
}

/*function for AI*/
void copy_board(char* b1,char* b2)
{
  int i;
  int j;
  for (i = 0; i < BOARD_SIZE; i++) {
    for (j = 0; j < BOARD_SIZE; j++) {
      b2[j * BOARD_SIZE + i] =  b1[j * BOARD_SIZE + i];
    }
  }
}

/*function for AI “smart” random*/
char ai_random(struct Game *game)
{ 
 int worthcolor[7]= {0};
 char chosencolor='o';
 int j;
 int i;
 char playerColor;
 if ((*(game->current_player)).id == 1) {playerColor = '1';} else {playerColor = '2';}
 int r_num=rand() % 7;
 for (i = 0; i < BOARD_SIZE; i++) {
        for (j = 0; j < BOARD_SIZE; j++) {
            if (check_adj(i, j, playerColor, game -> board)){
            worthcolor[colorev(get_cell(i, j, game -> board))]=1;
            }
         }
    };
    while(chosencolor=='o') {
        if (worthcolor[r_num] != 0)
        {
            chosencolor=colors(r_num);
        }
        else
        {
            r_num=rand() % 7;
        }
    }
    return chosencolor;
}

/*function for gluttony*/
char ai_glutton(struct Game * game)
{ 
  char * board = game -> board;
  char copyBoard[BOARD_SIZE * BOARD_SIZE] = {0};
  copy_board(board,copyBoard);
  
  int copyTerritory = (*(game->current_player)).territory;
 
  char bestToPlay = colors(0);
  
  int maxTerritory = copyTerritory;
 
  int i;
  for (i = 0; i < 7; i++)
    {
      actualize_world(colors(i),game);
      int territory = (*(game->current_player)).territory;
      
      if (territory > maxTerritory)
    {
      bestToPlay = colors(i);
      maxTerritory = territory;
    }
      
      (*(game->current_player)).territory = copyTerritory;
      copy_board(copyBoard, board);
    }
 
  return bestToPlay;
}

/*function for hegemony*/
char ai_hegemony(struct Game * game)
{
  char * board = game -> board;
  char copyBoard[BOARD_SIZE * BOARD_SIZE] = {0};
  copy_board(board,copyBoard);
  
  char playerColor;
  if ((*(game->current_player)).id == 1) {playerColor = '1';} else {playerColor = '2';}
  
  char bestToPlay = colors(0);
  int maxsum = 0;
  int k;
  int i;
  int j;
  int sum;
  
  int copyTerritory = (*(game->current_player)).territory;
  for (k = 0; k < 7 ; k++)
    {
      actualize_world(colors(k), game);
      sum=0;
      for (i = 0; i < BOARD_SIZE; i++) {
        for (j = 0; j < BOARD_SIZE; j++) {
            if ( check_adj(i, j, playerColor, board)){
                sum++;
            }
        }
    }
      if (sum > maxsum || (sum ==maxsum && (*(game->current_player)).territory > copyTerritory))
    {
      bestToPlay = colors(k);
      maxsum = sum;
    }

      (*(game->current_player)).territory = copyTerritory;
      copy_board(copyBoard, board);
    }

  return bestToPlay;
}

/* victory treatment */
void is_over(struct Game * game){
  if ((*(game->current_player)).territory >= BOARD_SIZE*BOARD_SIZE/2)
  {
      printf("Player%d wins! \n", ((game->current_player))->id);
      game->end = 1;
  }
}

/*asks if an other game is wished*/
char ask_new_game()
{
  char answer='0';
  printf("Play again? [Y/N] \n ");
  scanf("%c", &answer);
  getchar();
  return answer;
}

/*initialize the game */
struct Game * init_game(int dont_have, char board[])
{
  struct Game * game = malloc(sizeof(struct Game));
  
  char playertype;
  
  printf("choose player's type : H for human , r for random ,g for glutton ,h for hegemony\n(you can use these rules during the game too)\n");
  printf("Your type ");
  scanf("%c",&playertype);
  getchar();
  game->end = 0;
  if(dont_have)
  {
    srand(time(NULL));
    init_board(game->board);
    (game -> player1).id = 1;
    (game -> player1).type = playertype;
    (game -> player1).territory = 1;
    
    (game -> player2).id = 2;
    (game -> player2).type = 'O';
    (game -> player2).territory = 1;
  }
  else
  {
    copy_board(board, game->board);
    (game -> player1).id = 1;
    (game -> player1).type = 'O';
    (game -> player1).territory = 1;
    
    (game -> player2).id = 2;
    (game -> player2).type = playertype;
    (game -> player2).territory = 1;
  }
  
  
  printf("\n\nWelcome to the 7 wonders of the world of the 7 colors\n"
     "*****************************************************\n\n"
     "Current board state:\n");
  print_board(game -> board);
  return game;
}

/*returns the chosen color or f if the game is over*/
char my_turn(struct Game * game, struct Player * current_player)
{  
  char chosencolor='@';
  game->current_player = current_player;
  if ((game -> current_player)->type == 'H')
  {
      scanf("%c", &chosencolor);
      getchar();
      
      while (chosencolor == '1' || chosencolor == '2')
      {
          scanf("%c", &chosencolor);
          getchar();
      };
  }
  
  if ((game -> current_player)->type == 'r')
  {
      chosencolor = 'r';
  }
  
  if ((game -> current_player)->type == 'g')
  {
      chosencolor = 'g';
  }
  
  if ((game -> current_player)->type == 'h')
  {
      chosencolor = 'h';
  }
     
  if (chosencolor == 'r'){
      chosencolor = ai_random(game);
  }
  if (chosencolor == 'h'){
      chosencolor = ai_hegemony(game);
  }
  if (chosencolor =='g'){
      chosencolor = ai_glutton(game);
  }  
 
  actualize_world(chosencolor, game);
  printf("\n");
  printf("player 1's territory : %f %%\n", territory_percentage(&(game -> player1)));
  printf("player 2's territory : %f %%\n\n", territory_percentage(&(game -> player2)));
  print_board(game->board);
  is_over(game);
  return chosencolor; 
}

/*actualizes the board according to the opponent's turn*/
char opponent_turn(struct Game * game, struct Player * current_player, char opponent_color)
{
  game->current_player = current_player;
  actualize_world(opponent_color, game);
  printf("\n");
  printf("player 1's territory : %f %%\n", territory_percentage(&(game -> player1)));
  printf("player 2's territory : %f %%\n\n", territory_percentage(&(game -> player2)));
  print_board(game->board);
  is_over(game);
  return 'c';
}

/*free the game*/
void free_game(struct Game * game)
{
  free(game);
}

