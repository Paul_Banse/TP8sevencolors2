#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "functions.h"
#include "7colors.h"

/*recieves a board from the server*/
static char * receive_board(int server_fd)
{
  char * board = malloc(BOARD_SIZE * BOARD_SIZE);
  int size = receive(server_fd, board);
  if  (size < BOARD_SIZE * BOARD_SIZE)
  {
    fatal_error("Transmission over.");
  }
  return board;
}

/*watcher's main loop*/
int main(int argc, char * argv[])
{
  struct sockaddr_in SA;
  int fd;
  
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (fd < 0)
  {
    fatal_error("socket failed");
  }
  
  if (argc <= 2)
  {
    fatal_error("no adress or no port");
  }
  else
  {
    SA.sin_family = AF_INET;
    SA.sin_addr.s_addr = inet_addr(argv[1]);
    SA.sin_port = htons(atoi(argv[2]));
  }
  
  if (connect(fd, (struct sockaddr *)&SA, sizeof SA) < 0)
  {
    fatal_error("conect failed");
  }
  
  send_fd(fd, "W", sizeof(char));
  int test = 1;
  while(test)
  {
    char * board = receive_board(fd);
    if (board[0]=='*')
    {
      free(board);
      
      return 0;
    }
    else
    {
      print_board(board);
    }
  }
}

