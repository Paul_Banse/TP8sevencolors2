#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "functions.h"
#include "7colors.h"

/*recieves a board from the server */
static void receive_board(int server_fd, char * board1)
{
  char * board2 = malloc(BOARD_SIZE * BOARD_SIZE);
  int size = receive(server_fd, board2);
  if  (size < BOARD_SIZE * BOARD_SIZE)
  {
    fatal_error("The server do not respond, try again in a few minutes");
  }
  copy_board(board2, board1);
  free(board2);
}

/*recieves the chosen color from the server*/
static char receive_color(int server_fd)
{
  char * color = malloc(sizeof(char));
  int size = receive(server_fd, color);
  if  (size < 1)
  {
    fatal_error("The server do not respond, try again in a few minutes");
  }
  char c = color[0];
  free(color);
  return c;
}

/*sends the chosen color to the server*/
static void send_color(int server_fd, char * color)
{
  send_fd(server_fd, color, sizeof(char));
}

int main(int argc, char * argv[])
{
  struct sockaddr_in SA;
  int fd;
  
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (fd < 0)
  {
    fatal_error("socket failed");
  }
  if (argc <= 2)
  {
    fatal_error("no adress or no port");
  }
  else
  {
    SA.sin_family = AF_INET;
    SA.sin_addr.s_addr = inet_addr(argv[1]);
    SA.sin_port = htons(atoi(argv[2]));
  }
  
  if (connect(fd, (struct sockaddr *)&SA, sizeof SA) < 0)
  {
    fatal_error("conect failed");
  }
  
  send_color(fd, "P");
  char board[BOARD_SIZE * BOARD_SIZE];
  receive_board(fd, board);
  struct Game * game = init_game(0, board);
  char my_color = 's';
  send_color(fd, &my_color);
  char color_player;
  while(my_color != 'f')
  {
    printf("Waiting server \n");
    color_player = receive_color(fd);
    opponent_turn(game, &(game->player1), color_player); 
    
    if(game->end)
    {
      my_color='f';
    }
    else{
      printf("you turn\n");
      my_color = my_turn(game, &(game->player2));
      send_color(fd, &my_color);
      if (game->end)
      {
        my_color = 'f';
      }
    }
  }
  free_game(game);
  return 0;
}

