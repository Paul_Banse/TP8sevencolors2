#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "functions.h"
#include "7colors.h"
#include <time.h>

/*It prints the adress of the client*/
static void display_addr(struct sockaddr_in * sac)
{
  char *s;
  s = inet_ntoa(sac->sin_addr);
  if(!s)
  {
    return ;
  }
  printf("%s \n", s);
}

/*We try to accept a client */
static int accept_client(int fd)
{
  int r;
  struct sockaddr_in sac;
  socklen_t client_len = sizeof(sac);
  r = accept(fd, (struct sockaddr *)&sac, &client_len);
  if (r < 0)
  {
    fatal_error("accept failed");
  }
  display_addr(&sac);
  return r;
}

/*sends a board to the watcher*/
static void send_board(int client_fd, struct Game * game)
{
  send_fd(client_fd, game->board, sizeof(game->board));
}

/*sends color to the player*/
static void send_color(int client_fd, char * color)
{
  send_fd(client_fd, color, 1);
}

/*recieves color from the player*/
static char receive_color(int client_fd)
{
  char * color = malloc(sizeof(char));
  int size = receive(client_fd, color);
  if  (size < 1)
  {
    fatal_error("The Game is over");
  }
  char c = color[0];
  free(color);
  return c;
}

/*server's main loop*/
int main(int argc, char * argv[])
{
  struct sockaddr_in SA;
  int fd;
  int r;
  if (argc == 1)
  {
    fatal_error("no port");
  }
  else
  {
    SA.sin_family = AF_INET;
    SA.sin_addr.s_addr = htonl(INADDR_ANY);
    SA.sin_port = htons(atoi(argv[1]));
  }
  
  fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (fd < 0)
  {
    fatal_error("socket failed");
  }
  
  r = bind(fd, (struct sockaddr*)&SA, sizeof(SA));
  if (r < 0)
  {
    fatal_error("bind failed");
  }
  
  int l = listen(fd, 1);
  if(l < 0)
  {
    fatal_error("listen failed");
  }
  
  printf("Waiting for a player ...\n");
  int player_fd;
  player_fd = accept_client(fd);
  if(receive_color(player_fd) != 'P')
  {
    fatal_error("The client is not a player");
  }
  
  printf("Waiting for a watcher ...\n");
  int watcher_fd;
  watcher_fd = accept_client(fd);
  if(receive_color(watcher_fd) != 'W')
  {
    fatal_error("The client is not a watcher");
  }
  
  /*the game begins*/ 
  struct Game * game = init_game(1, "");
  send_board(player_fd, game);
  send_board(watcher_fd, game);
  printf("Waiting client\n");
  clock_t t1;
  clock_t t2;
  time(&t1);
  char my_color = 's';
  double time_server = 0.;
  char color_player = 'w';
  double time_player = 0.;
  
  while(color_player != 's')
  {
    color_player = receive_color(player_fd);
  }
  while(my_color != 'f')
  {
	  time(&t1);
    printf("Your turn\n");
    my_color = my_turn(game, &(game->player1));
    send_color(player_fd, &my_color);
    send_board(watcher_fd, game);
    time(&t2);
    time_server += t2-t1;
    printf("time spent by the server %f \n", (double) (t2-t1));
    if(game->end)
    {
      my_color='f';
    }
    else{
      time(&t1);
      printf("Waiting client \n");
      color_player = receive_color(player_fd);
      opponent_turn(game, &(game->player2), color_player); 
      send_board(watcher_fd, game);
      time(&t2);
      time_player += t2-t1;
      printf("time spent by the other player %f \n",(double) (t2-t1));
      if (game->end)
      {
        my_color='f';
      }
   }
  }
  printf("Time Results :\nServer : %f  Client : %f\n", time_server, time_player);
  free_game(game);
  return 0;
}


